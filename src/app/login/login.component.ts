import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  defaultId = 'admin';
  defaultPass = '12345678';
  notifikasi: string;

  constructor(
    private fb: FormBuilder,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      id: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  onSubmit(): void{
    if (
      this.loginForm.value.id === this.defaultId ||
      this.loginForm.value.password === this.defaultPass
    ) {
      this.router.navigate(['/home']);
      console.log('Yeyy Berhasil Login');
    } else {
      this.notifikasi = "Gagal Login"
    }
  }
}
